import model.db_fonction as db_f
from fastapi import FastAPI, Query, Path
from pydantic import BaseModel
from typing import Union

# uvicorn app:app --reload

conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
cur = conn.cursor()
app = FastAPI()

#----------------------------------------------------#
#------- Déclaration des fonctions pour l'API -------#
#----------------------------------------------------#

#------- produit -------#
class reference(BaseModel):
    reference: str | None = None
    expendable: bool = False

@app.post("/add_material/{reference}")
def add_material(reference: Union[str, None] = None, expendable: bool = False, designation: Union[str, None] = None, description: Union[str, None] = None):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
    result = []
    id_material = db_f.insert_material(cursor,reference, expendable)
    if id_material:
        result.append(f"id_material: {id_material}")
    if id_material:
        id_designation = db_f.insert_designation(cursor, id_material, designation)
        if id_designation:
            result.append(f"id_designation: {id_designation}")
        id_description = db_f.insert_description(cursor,id_material, description)
        if id_description:
            result.append(f"id_designation: {id_description}")
    conn.commit()
    conn.close()
    return result
# http://127.0.0.1:8000/add_material/'RFDLS22'?designation=un truc de merde&description=un truck à la con pour teste fastapi

@app.get("/search_mat/")
@app.get("/search_mat/{search}")
def add_material(search:str = None):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
    param = ''
    if search:
        param = f"""AND(
                        reference LIKE '%{search}%'
                        OR
                        designation LIKE '%{search}%'
                        OR
                        description LIKE '%{search}%')"""
    query = f"""
            SELECT 
                mat.id,
                mat.reference,
                desi.designation,
                desc.description
            FROM
                material AS mat
            LEFT JOIN
                description AS desc 
                    ON mat.id = desc.id_material AND desc.deleted = 0
            LEFT JOIN
                designation AS desi
                    ON mat.id = desi.id_material AND desi.deleted = 0
            WHERE
                mat.deleted = 0 
                {param}
            """
    result = db_f.execute_query(query,cursor)
    conn.commit()
    conn.close()
    return result

@app.delete("/delete_mat/{id_material}")
def delete_mat(id_material:int):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
    db_f.delete_material(cursor, id_material)
    conn.commit()
    conn.close()
    
#------- category -------#
@app.post("/create_category/{name}")
def creat_category(name, id_parent: Union[int, None] = None):
    if name : 
        conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
        cursor = conn.cursor()
        id_category = db_f.insert_category(cursor,name,id_parent)
        conn.commit()
        conn.close()
        return id_category
    
@app.get("/search_category/")
@app.get("/search_category/{category}")
def search_category(category: Union[str, None] = None):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
    param = ""
    if category:
        category = db_f.norm_designation(category)
        param = "WHERE standardized_name LIKE '%{category}%'"
    query = f"""SELECT 
                    id,
                    name,
                    category_parent,
                    standardized_name
                FROM 
                    category
                {param};"""
    result = db_f.execute_query(query,cursor)
    print(result)
    conn.commit()
    conn.close()
    return result

#------- transaction -------#
#------- person -------#
@app.get("/search_pers/")
@app.get("/search_pers/{search}")
def search_pers(search: Union[str, None] = None):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
    param= ''
    if search:
        param = f"""AND(
                   per.matricule LIKE '%{search}%'
                OR
                    ide.first_name LIKE '%{search}%'
                OR
                    ide.last_name LIKE '%{search}%'
                OR
                    regiment LIKE '%{search}%'
                OR 
                    compagny LIKE '%{search}%'
                OR
                    section LIKE '%{search}%')"""
    query = f"""
                SELECT 
                    per.matricule,
                    ide.first_name,
                    ide.last_name,
                    ide.regiment,
                    ide.compagny,
                    ide.section
                FROM 
                    identity AS ide
                JOIN 
                    person AS per ON ide.id_person = per.id
                WHERE
                    deleted = 0
                {param}
            """
    datas = db_f.execute_query(query,cursor)
    conn.commit()
    conn.close()
    return datas

@app.get("/average_delay_per_person/{matricule}")
def average_delay_per_person(matricule: str):
    
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
            
    query = f"""WITH borrowed_close AS (
                SELECT 
                    bor.id AS borrowed_id,
                    bor.loan_date,
                    bor.deadline,
                    bor.return_date,
                    bor.status,
                    bom.id_material,
                    bor.id_person,
                    round(julianday(bor.return_date) - julianday(bor.deadline)) AS delay
                FROM 
                    borrowed AS bor
                JOIN
                    borrowed_material AS bom 
                    ON bor.id = bom.id_borrowed
                WHERE
                    bor.status = 'close'
                    AND bor.deadline IS NOT NULL 
                    AND bor.return_date IS NOT NULL
                )

            SELECT
                per.matricule,
                ide.first_name,
                ide.last_name,
                ide.regiment,
                ide.compagny,
                ide.section,
                AVG(boc.delay) AS average_delay
            FROM
                borrowed_close AS boc
            JOIN
                person AS per
                ON boc.id_person = per.id
            JOIN
                identity AS ide 
                ON per.id = ide.id_person
            WHERE
                per.deleted = 0
                AND per.matricule like '%{matricule}%'
            GROUP BY
                per.matricule;
            """
    datas = db_f.execute_query(query, cursor)
    conn.commit()
    conn.close()
    if datas:
        return datas       

#-----------------------------------#
#------- Appel des donctions -------#
#-----------------------------------#

# print(average_delay_per_person(cur,first_name='juli', compagny='1 ccg'))


conn.commit()
conn.close()