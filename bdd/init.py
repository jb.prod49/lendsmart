import sqlite3
from sqlite3 import Error

conn = sqlite3.connect("bdd/lendsmart_bdd.db")
cur = conn.cursor()

#--------------------------------------------------#
#------- Déclaration des fonctions pour BDD -------#
#--------------------------------------------------#
def connect_sqlite(bdd_name):
    return sqlite3.connect(bdd_name+".db")
        
def execute_query(query: str, cursor, params=None, fetch=True):
    try:
        cursor.execute(query, params or ())
        result = cursor.fetchall() if fetch else None
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        print(f"query : {query}")
        return None

def execute_insert(query: str, values: tuple, cursor)->int:
    try:
        cursor.execute(query, values)
        return cursor.lastrowid
    except sqlite3.IntegrityError:
        return None

#---------------------------------------------------#
#-------- création de la BDD avec SQLITE3 ----------#
#---------------------------------------------------#
print("#------ Création de la BDD ------#")
list_query = [
        """
            DROP TABLE IF EXISTS `material`;
        """,
        """
            CREATE TABLE `material` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `reference` VARCHAR(50) NOT NULL DEFAULT 'unknown',
                `expendable` BOOL NOT NULL DEFAULT false CHECK(expendable IN (0, 1)),
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `deleted` BOOL NOT NULL DEFAULT false CHECK(deleted IN (0, 1)), 
                `date_delete` DATETIME);
        """,
        """
            CREATE TRIGGER `delete_material` 
            AFTER UPDATE OF `deleted` ON `material` FOR EACH ROW
            BEGIN
                UPDATE `material` SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
                UPDATE `validity_date` SET deleted = 1 WHERE `id_material` = OLD.id;
                UPDATE `designation` SET deleted = 1 WHERE `id_material` = OLD.id;
                UPDATE `description` SET deleted = 1 WHERE `id_material` = OLD.id;
                DELETE FROM mat_cat WHERE `id_material` = OLD.id;
            END;
        """,
        """
            DROP TABLE IF EXISTS `validity_date`;
        """, 
        """
            CREATE TABLE `validity_date` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `id_material` INTEGER NOT NULL,
                `validity_date` DATETIME NOT NULL,
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `deleted` BOOL NOT NULL DEFAULT false CHECK(deleted IN (0, 1)),
                `date_delete` DATETIME,
                FOREIGN KEY (`id_material`) REFERENCES `material` (`id`));
        """,
        """
            CREATE TRIGGER `delete_validity_date` 
            AFTER UPDATE OF `deleted` ON `validity_date` FOR EACH ROW
            BEGIN
                UPDATE `validity_date` 
                SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """
            CREATE TRIGGER before_insert_validity_date
            BEFORE INSERT ON `validity_date` FOR EACH ROW
            BEGIN
                UPDATE `validity_date`
                SET `deleted` = 1,
                    `date_delete` = CURRENT_TIMESTAMP
                WHERE `id_material` = NEW.id_material AND `id` != NEW.id;
            END;
        """,
        """
            DROP TABLE IF EXISTS `designation`;
        """,        
        """                
            CREATE TABLE `designation` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `id_material` INTEGER NOT NULL,
                `designation` VARCHAR(255) NOT NULL DEFAULT '',
                `standardized_designation` VARCHAR(255) NOT NULL DEFAULT '',
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `deleted` BOOL NOT NULL DEFAULT false CHECK(deleted IN (0, 1)), 
                `date_delete` DATETIME,
                FOREIGN KEY (`id_material`) REFERENCES `material` (`id`));
        """,
        """
            CREATE TRIGGER `delete_designation` 
            AFTER UPDATE OF `deleted` ON `designation` FOR EACH ROW
            BEGIN
                UPDATE `designation` 
                SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """
            CREATE TRIGGER before_insert_designation
            BEFORE INSERT ON `designation`
            FOR EACH ROW
            BEGIN
                UPDATE `designation`
                SET `deleted` = 1,
                    `date_delete` = CURRENT_TIMESTAMP
                WHERE `id_material` = NEW.id_material AND `id` != NEW.id;
            END;
        """,
        """
            DROP TABLE IF EXISTS `description`;
        """,        
        """                
            CREATE TABLE `description` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `id_material` INTEGER NOT NULL,
                `description` TEXT NOT NULL DEFAULT '',
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `deleted` BOOL NOT NULL DEFAULT false CHECK(deleted IN (0, 1)), 
                `date_delete` DATETIME,
                FOREIGN KEY (`id_material`) REFERENCES `material` (`id`));
        """,
        """
            CREATE TRIGGER `delete_description` 
            AFTER UPDATE OF `deleted` ON `description` FOR EACH ROW
            BEGIN
                UPDATE `description` 
                SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """
            CREATE TRIGGER before_insert_description
            BEFORE INSERT ON `description`
            FOR EACH ROW
            BEGIN
                UPDATE `description`
                SET `deleted` = 1,
                    `date_delete` = CURRENT_TIMESTAMP
                WHERE `id_material` = NEW.id_material AND `id` != NEW.id;
            END;
        """,
        """
            DROP TABLE IF EXISTS `category`;
        """,
        """           
            CREATE TABLE `category` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `name` VARCHAR(50) UNIQUE NOT NULL,
                `standardized_name` VARCHAR(255) UNIQUE NOT NULL,
                `category_parent` INTEGER,
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                FOREIGN KEY (`category_parent`) REFERENCES `category` (`id`));
        """,       
        """
            CREATE TRIGGER `delete_category` 
            AFTER DElETE ON `category` FOR EACH ROW
            BEGIN
                UPDATE `category` SET `category_parent` = NULL WHERE `category_parent` = OLD.id;
                DELETE FROM `mat_cat` WHERE `id_category` =  OLD.id;
            END;
        """,
        """
            DROP TABLE IF EXISTS `mat_cat`;
        """,
        """
        CREATE TABLE `mat_cat` (
            `id_material` INTEGER NOT NULL,
            `id_category` INTEGER NOT NULL,
            FOREIGN KEY (`id_material`) REFERENCES `material` (`id`),
            FOREIGN KEY (`id_category`) REFERENCES `category` (`id`),
            PRIMARY KEY(`id_material`,`id_category`));
        """,
        """
            DROP TABLE IF EXISTS `person`;
        """,
        """
            CREATE TABLE `person` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `matricule` VARCHAR(15) NOT NULL UNIQUE,
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `deleted` bool NOT NULL DEFAULT false CHECK(deleted IN (0, 1)),
                `date_delete` DATETIME);
        """,
        """
            CREATE TRIGGER `delete_person` 
            AFTER UPDATE OF `deleted` ON `person` FOR EACH ROW
            BEGIN
                UPDATE `person` SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
                DELETE FROM `identity` WHERE `id_person` =  OLD.id;
            END;
        """,
        """       
            DROP TABLE IF EXISTS `identity`;
        """,
        """
            CREATE TABLE `identity` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `id_person` INTEGER NOT NULL,
                `grade` VARCHAR(3),
                `first_name` VARCHAR(100),
                `last_name` VARCHAR(100),
                `sex` VARCHAR(1),
                `regiment` VARCHAR(50),
                `compagny` VARCHAR(50),
                `section` VARCHAR(250),
                `phone` INTEGER(10),
                `email` VARCHAR(250),
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `date_update` DATETIME,
                FOREIGN KEY (`id_person`) REFERENCES `person` (`id`));
        """,
        """
            CREATE TRIGGER `update_identity` 
            AFTER UPDATE  ON `identity`FOR EACH ROW
            BEGIN
                UPDATE `identity` SET `date_update` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """       
            DROP TABLE IF EXISTS `size`;
        """,
        """
            CREATE TABLE `size` (
                `id` INTEGER PRIMARY KEY NOT NULL,
                `id_person` INTEGER NOT NULL,
                `hand_width` DECIMAL,
                `head_measurement` DECIMAL,
                `waist_size` DECIMAL,
                `inseam_length` DECIMAL,
                `person_size` DECIMAL,
                `chest_size` DECIMAL,
                `shose_size` DECIMAL,
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `date_update` DATETIME,
                FOREIGN KEY (`id_person`) REFERENCES `person` (`id`));
        """,
        """       
            DROP TABLE IF EXISTS `borrowed`;
        """,
        """
            CREATE TABLE `borrowed` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `id_person` INTEGER NOT NULL,
                `loan_date` DATETIME NOT NULL DEFAULT current_timestamp,
                `deadline` DATETIME,
                `return_date` DATETIME,
                `status` TEXT NOT NULL CHECK(status IN ('on hold','accepted', 'refused', 'in progress', 'delay', 'close', 'cancel')) DEFAULT 'on hold',
                `date_create` DATETIME NOT NULL DEFAULT current_timestamp,
                `date_status` DATETIME ,
                FOREIGN KEY (`id_person`) REFERENCES `person` (`id`));
        """,
        """
            CREATE TRIGGER `delete_borrowed` 
            AFTER UPDATE OF `status` ON `borrowed`FOR EACH ROW
            BEGIN
                UPDATE `borrowed` SET `date_status` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """       
            DROP TABLE IF EXISTS `borrowed_material`;
        """,
        """
            CREATE TABLE `borrowed_material` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `id_material` INTEGER NOT NULL,
                `id_borrowed` INTEGER NOT NULL,
                `deleted` bool NOT NULL DEFAULT false CHECK(deleted IN (0, 1)),
                `date_delete` DATETIME,
                FOREIGN KEY (`id_material`) REFERENCES `material` (`id`),
                FOREIGN KEY (`id_borrowed`) REFERENCES `borrowed` (`id`));
        """,
        """
            CREATE TRIGGER `delete_borrowed_material` 
            AFTER UPDATE OF `deleted` ON `borrowed_material` FOR EACH ROW
            BEGIN
                UPDATE `borrowed_material` SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """       
            DROP TABLE IF EXISTS `status_report`;
        """,
        """
            CREATE TABLE `status_report` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `date_creat` DATETIME NOT NULL DEFAULT current_timestamp,
                `text_report` TEXT NOT NULL,
                `id_material` INTERGER, 
                `id_borrowed` INTEGER,
                `id_borrowed_material` INTEGER, 
                `deleted` bool NOT NULL DEFAULT false CHECK(deleted IN (0, 1)),
                `date_delete` DATETIME,
                FOREIGN KEY (`id_material`) REFERENCES `material` (`id`),
                FOREIGN KEY (`id_borrowed`) REFERENCES `borrowed` (`id`),
                FOREIGN KEY (`id_borrowed_material`) REFERENCES `borrowed_material` (`id`));   
        """,
        """
            CREATE TRIGGER `delete_status_report` 
            AFTER UPDATE OF `deleted` ON `status_report` FOR EACH ROW
            BEGIN
                UPDATE `status_report` SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """       
            DROP TABLE IF EXISTS `comment`;
        """,
        """
            CREATE TABLE `comment` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `id_borrowed` INTERGER NOT NULL,
                `text_comment` TEXT NOT NULL, 
                `date_creat` DATETIME NOT NULL DEFAULT current_timestamp,
                `deleted` bool NOT NULL DEFAULT false CHECK(deleted IN (0, 1)),
                `date_delete` DATETIME,
                FOREIGN KEY (`id_borrowed`) REFERENCES `borrowed` (`id`));   
        """,
        """
            CREATE TRIGGER `delete_comment` 
            AFTER UPDATE OF `deleted` ON `comment` FOR EACH ROW
            BEGIN
                UPDATE `comment` SET `date_delete` = current_timestamp WHERE `id` = OLD.id;
            END;
        """,
        """
            PRAGMA foreign_keys = ON;
        """
        ]

for query in list_query:
    execute_query(query, cur)
conn.commit()
conn.close() 
print("#------ Fin de la création ------#")