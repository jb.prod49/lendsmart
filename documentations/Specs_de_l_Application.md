
# Spécifications de l'Application LendSmart

## Introduction

L'application LendSmart est conçue pour gérer le prêt de matériel au sein d'organisations telles que les institutions militaires, où le suivi rigoureux des ressources est crucial. Elle assure un suivi précis des articles depuis leur demande jusqu'à leur retour, en gérant les stocks, la validité des équipements et en centralisant les informations relatives aux emprunteurs.

## Entités de la Base de Données

### Material

Chaque item de matériel est enregistré avec des détails tels que sa référence, la date de création et un indicateur de consommabilité.

### Validity_Date

Gère les dates de validité de chaque matériel, permettant de suivre l'échéance d'utilisation des articles.

### Designation & Description

Fournissent des informations descriptives sur le matériel, y compris des désignations standardisées pour une reconnaissance et une catégorisation uniformes.

### Category & Mat_Cat

Permettent de classer le matériel en différentes catégories, facilitant leur recherche et leur gestion.

### Person & Identity & Sizes

Enregistrent les détails des individus qui empruntent le matériel, incluant des informations personnelles et des dimensions pour les équipements spécifiques comme les vêtements ou les EPI.

### Borrowed & Borrowed_Material

Suivent les transactions de prêt, enregistrant les détails des emprunts et les matériaux associés.

### Status_Report & Comment

Permettent la génération de rapports sur l'état du matériel après emprunt et offrent une fonctionnalité pour commenter les transactions.

## Processus de Gestion des Prêts

1. **Demande de Matériel** :
   * L'utilisateur fait une demande d'emprunt via l'application.
   * Un ticket est créé, son statut étant initialement sur "en attente".
2. **Vérification de la Disponibilité** :
   * Le système vérifie la disponibilité de l'objet demandé.
   * Si l'article n'est pas disponible, le statut passe à "refusé".
3. **Décision de Prêt** :
   * Si l'objet est disponible et en bon état, une fiche d'emprunt est créée.
   * Le matériel est fourni à l'utilisateur et la quantité disponible est mise à jour.
4. **Suivi du Prêt** :
   * Les dates de retour sont définies et le système surveille les échéances.
   * Les retours sont enregistrés et vérifiés, et le matériel est réintégré au stock.
5. **Gestion des Incidents** :
   * En cas de retard ou de matériel non retourné, un suivi spécial est initié.
   * Des rapports d'incident sont créés pour les articles endommagés ou non retournés.

## Technologies Proposées

* **Backend** : Python, Pandas, FastAPI pour le développement de l'API.
* **Frontend** : HTML et CSS pour l'interface graphique.
* **Base de Données** : SQLite.

## Fonctionnalités de l'Application

* **Interface utilisateur intuitive** pour la gestion des demandes et le suivi des prêts.
* **Tableaux de bord dynamiques** pour une visualisation rapide de l'état des stocks et des prêts.
* **Notifications automatiques** pour les dates de validité et les retours en retard.
* **Rapports détaillés** pour la maintenance du matériel et les besoins de réapprovisionnement.

L'application LendSmart est une solution complète qui vise à révolutionner la manière dont les ressources matérielles sont gérées dans des contextes exigeants, en garantissant l'efficacité, la sécurité et la responsabilité dans le processus de prêt.

## Normalisation des Données

Toutes les dates seront enregistrées au format AAAA/MM/JJ HH:MM:SS.

### Module de Gestion de Stock

#### Table material :

* reference :
  * Sans caractères spéciaux
  * Minuscule et majuscule
  * Alphanumérique

#### Table Designation :

* designation :
  * Sauvegarde de valeur sans modification
* standardized_designation :
  * Sans caractères spéciaux
  * En minuscule

#### Table Description :

* description :
  * Sauvegarde de valeur sans modification

#### Table category :

* name :
  * Sauvegarde de valeur sans modification
* standardized_name:
  * Sans caractères spéciaux
  * En minuscule

#### Table person :

* matricule :
  * Sans caractères spéciaux
  * Minuscule et majuscule
  * Alphanumérique

#### Table identity :

* first_name :
  * Sauvegarde de valeur sans modification
* last_name :
  * Sauvegarde de valeur sans modification
* adress varchar :
  * Sauvegarde de valeur sans modification
* zipcode :
  * Entier uniquement
  * Maximum 5 chiffres
* city :
  * Sauvegarde de valeur sans modification
* phone :
  * Entier uniquement
  * Maximum 10 chiffres
* email :
  * Format email

#### Table size :

* hand_width :
  * Taille en cm
* head_measurement :
  * Taille en cm
* waist_size :
  * Taille en cm
* inseam_length :
  * Taille en cm
* person_size :
  * Taille en cm
* chest_size :
  * Taille en cm
* Shose_size decimal :
  * Taille en cm

#### Table borrowed :

* loan_date :
  * Date prévue de perception
* deadline :
  * Date prévue de réintégration
* return_date :
  * Date réelle de la réintégration
* status :
  * Les différents statuts possibles : "en attente", "accepté", "refusé", "en cours", "en retard", "clôturé"

#### Table status_report :

* text_report :
  * Sauvegarde de valeur sans modification

#### Table comment :

* text_comment :
  * Sauvegarde de valeur sans modification
