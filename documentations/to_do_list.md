# TO DO LIST

## Planification et Conception

* Rédiger les User Stories pour chaque fonctionnalité de l'application.
* Concevoir le schéma de la base de données et les relations entre les tables.
* Établir les règles de gestion des données et les contraintes d'intégrité.
* Créer des wireframes pour les interfaces utilisateur principales.

## Environnement et Configuration

* Configurer le repository Git pour le versioning du code.
* Choisir et configurer l'environnement de développement local.
* Définir la stratégie de déploiement et les outils nécessaires.

## Configuration de l'Environnement

* Installer Python, Pandas, FastAPI et SQLite
* Configurer l'environnement de développement virtuel
* Configurer le contrôle de version avec Git

## Modèles de Base de Données

* Créer les classes de modèle pour la base de données SQLite avec l'ORM de FastAPI
* Écrire les fonctions de migration pour la création des tables
* Insérer des données initiales pour le test

## Fonctions de Gestion de Base de Données

* **Matériels**
  * Créer la fonction d'ajout de matériel (`add_material`)
  * Créer la fonction de mise à jour de matériel (`update_material`)
  * Créer la fonction de suppression de matériel (`delete_material`)
  * Créer la fonction pour lister et filtrer les matériels (`list_materials`)
* **Personnes**
  * Créer la fonction d'ajout de personne (`add_person`)
  * Créer la fonction de mise à jour de personne (`update_person`)
  * Créer la fonction de suppression de personne (`delete_person`)
  * Créer la fonction pour lister et filtrer les personnes (`list_people`)
* **Emprunts**
  * Créer la fonction d'ajout d'emprunt (`add_borrowed`)
  * Créer la fonction de mise à jour d'emprunt (`update_borrowed`)
  * Créer la fonction de clôture d'emprunt (`close_borrowed`)
  * Créer la fonction pour lister et filtrer les emprunts (`list_borrowed`)
* **Rapports**
  * Créer la fonction d'ajout de rapport d'état (`add_status_report`)
  * Créer la fonction de mise à jour de rapport d'état (`update_status_report`)
  * Créer la fonction pour lister et filtrer les rapports d'état (`list_status_reports`)
* **Commentaires**
  * Créer la fonction d'ajout de commentaire (`add_comment`)
  * Créer la fonction pour lister les commentaires (`list_comments`)

## Statistiques et Visualisations

* Définir les statistiques clés à générer
* Intégrer une bibliothèque de visualisation de données comme Matplotlib ou Seaborn

## Développement des API

* Implémenter les routes CRUD pour la gestion des matériels, personnes et emprunts
* Développer l'authentification et la gestion des sessions
* Créer les endpoints pour les statistiques et les rapports

## Intégration Frontend

* Créer des maquettes pour l'interface utilisateur
* Développer les pages HTML/CSS pour la gestion de l'inventaire, des utilisateurs et des prêts
* Intégrer les visualisations dans l'interface utilisateur

## Tests

* Écrire et exécuter des tests unitaires pour les modèles et les fonctions de gestion de base de données
* Écrire et exécuter des tests d'intégration pour les API
