import re
import xlrd
import random
from datetime import datetime, timedelta
# import datetime
import model.db_fonction as db_f

#-----------------------------------#
#---------- Les fonctions ----------#
#-----------------------------------#
def parse_datetime(date_str):
    for fmt in ("%Y-%m-%d %H:%M:%S", "%Y-%m-%d"):
        try:
            return datetime.strptime(date_str, fmt)
        except ValueError:
            continue
    raise ValueError(f"time data {date_str!r} does not match known formats")

def trouver_tailles(taille_informee:dict):
    taille_dict = {'x': (150, 160), 'xc': (150, 160), 'c': (160, 170),
                   'm': (160, 170), 'l': (180, 190), 'xl': (190, 215)}
    entre_jambe_dict = {'x': 60, 'xc': 60, 'c': 65, 'm': 70, 'l': 75, 'xl': 80}
    
    if taille_informee:
        match = re.search(r'(\d+)(?:/(\d+))?([a-zA-Z]+)', taille_informee)
        if match:
            min_num = int(match.group(1))
            max_num = int(match.group(2)) if match.group(2) else min_num
            number = random.randint(min_num, max_num)

            lettre = match.group(3).lower()
            person_size = None
            inseam_length = None
            
            if lettre in taille_dict:
                person_size_range = taille_dict[lettre]
                person_size = random.randint(person_size_range[0], person_size_range[1])
            if lettre in entre_jambe_dict:
                inseam_length = entre_jambe_dict[lettre]
            
            if number is not None and person_size is not None and inseam_length is not None:
                return [number, person_size, inseam_length]

    return None

def insert_info(matricule:int, grade:str, first_name:str, last_name:str, sex:str, compagny:str, hand_width:int, head_measurement:int, waist_size:int, inseam_length:int, person_size:int, chest_size,shose_size:int):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    conn.execute('PRAGMA writable_schema=ON')
    cur = conn.cursor()
    
    id_person = db_f.insert_person(cur, matricule)
    if id_person:
        db_f.insert_identity(cur, id_person=id_person, grade=grade, first_name=first_name, last_name=last_name, sex=sex, compagny=compagny)
        db_f.insert_size(cur,id_person=id_person, hand_width=hand_width,head_measurement=head_measurement,waist_size=waist_size,inseam_length=inseam_length,person_size=person_size,chest_size=chest_size,shose_size=shose_size)
    conn.commit()
    conn.close()
    return id_person
    
def insert_casque_felin(reference:str, id_person:int):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    if id_person:
        id_material = db_f.insert_material(cur, reference) 
        db_f.insert_designation(cur, id_material, "Casque composite félin") 
        id_category = db_f.execute_query("SELECT id FROM category WHERE name = 'HABILLEMENT';",cur)
        if not id_category:
            id_category = db_f.insert_category(cur,"HABILLEMENT")
        elif id_category:
                id_category = id_category[0][0]
        db_f.insert_mat_cat(cur,id_material,id_category)
        date = datetime.now().strftime("%Y-%m-%d") 
        id_borrowed = db_f.insert_borrowed(cur, id_person=id_person, loan_date=date, status='in progress')
        
        if id_borrowed:
            db_f.insert_borrowed_material(cur, id_material, id_borrowed)
    conn.commit()
    conn.close()
    
def insert_material(qt:int, reference:str, designation:str, category:str):
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    
    if not reference:
        reference = "unknown"
    x = 0
    while x < qt:
        id_material = db_f.insert_material(cur, reference)
        if designation:
            db_f.insert_designation(cur,id_material,designation)
        if category:
            id_category = db_f.select_category(cur,name_category=category)
            if not id_category:
               id_category = db_f.insert_category(cur,category)
            else:
                id_category = id_category[0][0]
            db_f.insert_mat_cat(cur,id_material,id_category)
        x += 1
    conn.commit()
    conn.close()
    
def generate_loans():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
    
    statuses = ['on hold', 'accepted', 'refused', 'in progress', 'delay', 'close', 'cancel']

    # Générer 500 prêts
    for _ in range(500):
    # Sélectionner un ID de personne au hasard
        cursor.execute("SELECT id FROM person ORDER BY RANDOM() LIMIT 1")
        person_id = cursor.fetchone()[0]
        
        # Calculer les dates de prêt et de retour avec des deadlines avant et après aujourd'hui
        days_before_today = random.randint(-60, 30)  
        loan_date = datetime.now() + timedelta(days=days_before_today)
        deadline_days = random.randint(1, 120) 
        deadline = loan_date + timedelta(days=deadline_days)
        
        # Sélectionner un statut au hasard parmi ceux disponibles
        status = random.choice(statuses)
        
        # Définir la date de retour uniquement si le statut est 'close'
        return_date = deadline + timedelta(days=random.randint(-10, 10)) if status == 'close' else None
        return_date_str = return_date.strftime('%Y-%m-%d %H:%M:%S') if return_date else None

        # Insérer le prêt dans la table 'borrowed'
        cursor.execute("""
            INSERT INTO borrowed (id_person, loan_date, deadline, return_date, status)
            VALUES (?, ?, ?, ?, ?)
        """, (person_id, loan_date.strftime('%Y-%m-%d %H:%M:%S'), deadline.strftime('%Y-%m-%d %H:%M:%S'), return_date_str, status))
        
        borrowed_id = cursor.lastrowid

        # Associer plusieurs matériels à la fiche de prêt
        for _ in range(random.randint(1, 5)):
            # Sélectionner un ID de matériel au hasard qui n'est pas actuellement en prêt
            cursor.execute("""
                SELECT id FROM material 
                WHERE id NOT IN (
                    SELECT id_material FROM borrowed_material
                    JOIN borrowed ON borrowed_material.id_borrowed = borrowed.id
                    WHERE borrowed.return_date IS NULL OR borrowed.status NOT IN ('on hold','close', 'cancel')
                )
                AND NOT deleted = 1
                ORDER BY RANDOM() 
                LIMIT 1
            """)
            material_result = cursor.fetchone()
            
            if not material_result:
                continue  # Aucun matériel disponible, passer au prochain matériel
            
            material_id = material_result[0]
            if material_id == None:
                print('material_id :' , material_id)
            # Associer le matériel au prêt dans la table 'borrowed_material'
            cursor.execute("""
                INSERT INTO borrowed_material (id_material, id_borrowed, deleted)
                VALUES (?, ?, 0)
            """, (material_id, borrowed_id))    
    conn.commit()
    conn.close()
    
def simulate_loan_status_change():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cursor = conn.cursor()
    
    for _ in range(100):
        query = "SELECT id, loan_date, deadline, status FROM borrowed WHERE status NOT IN ('close', 'cancel') ORDER BY RANDOM() LIMIT 1"
        cursor.execute(query)
        borrowed = cursor.fetchone()
        
        if borrowed:
            id_borrowed, loan_date_str, deadline_str, status = borrowed
            
            loan_date = parse_datetime(loan_date_str)
            deadline = parse_datetime(deadline_str) if deadline_str else None
            
            if status == 'on hold':
                if loan_date < datetime.now():
                    new_status = random.choice(['accepted', 'refused', 'cancel'])
                else:
                    new_status = random.choice(['in progress', 'close', 'cancel'])
            elif deadline and deadline < datetime.now():
                new_status = random.choice(['delay', 'close', 'cancel'])
            else:
                new_status = random.choice(['in progress', 'close', 'cancel'])
           
            if new_status == 'close':
                return_date = deadline + timedelta(days=random.randint(-10, 10)) if deadline else None
                return_date_str = f"'{return_date.strftime('%Y-%m-%d %H:%M:%S')}'" if return_date else "NULL"
            else:
                return_date_str = "NULL"
            
            query = f"UPDATE borrowed SET status = '{new_status}', return_date = {return_date_str} WHERE id = {id_borrowed}"
            cursor.execute(query)
    conn.commit()
    conn.close()
#--------------------------------------------------------------#
#---------- Insertion des personnes avec les tailles ----------#
#--------------------------------------------------------------#
filepath = "bdd/res/LISTE  TAILLE  972e CPE 27.11.18.xls"

book = xlrd.open_workbook(filepath)
sheet = book.sheet_by_index(0)

for rx in range(34, sheet.nrows):
    cellB = sheet.cell(rx, 1)
    cell_value = str(cellB.value).split(':')
    matricule = cell_value[0]
    if matricule:
        matricule = matricule.replace("'", "").replace(" ", "").replace(".0","")
    else: 
        matricule = None  
        
    cellD = sheet.cell(rx, 3)
    cell_value = str(cellD.value).split(':')
    if cell_value[0] == "1°CL" or cell_value[0] == "1 CL":
        grade = "1CL"
    else:
        grade = cell_value[0]

    cellE = sheet.cell(rx, 4)
    last_name = str(cellE.value).split(':')[0]
    
    cellF = sheet.cell(rx, 5)
    first_name = str(cellF.value).split(':')[0]
    
    cellG = sheet.cell(rx, 6)
    compagny = str(cellG.value).split(':')[0]

    cellH = sheet.cell(rx, 7)
    sex = str(cellH.value).split(':')[0]

    cellAA= sheet.cell(rx, 26)
    hand_width = str(cellAA.value).split(':')[0]
    if hand_width:
        hand_width = float(hand_width)
    else:
        hand_width = None
    
    cellP = sheet.cell(rx, 15)
    head_measurement = str(cellP.value).split(':')[0].replace(".0","")
    if head_measurement:
        head_measurement = float(head_measurement)
    else:
        head_measurement = None
    
    cellK = sheet.cell(rx, 10)
    jacket_size = str(cellK.value).split(':')[0].replace("'","")
    taille_resultat = trouver_tailles(jacket_size)
    if taille_resultat:
        chest_size = float(taille_resultat[0])
        
    cellL = sheet.cell(rx, 11)
    pant_size = str(cellL.value).split(':')[0].replace("'","")
    taille_resultat = trouver_tailles(pant_size)
    if taille_resultat:
        waist_size = float(taille_resultat[0])
        inseam_length = float(taille_resultat[2])
    
    if jacket_size:
        person_size = trouver_tailles(jacket_size)
        if person_size:
            person_size = float(person_size[1])
    elif pant_size:
        person_size = trouver_tailles(pant_size)
        if person_size:
            person_size = float(person_size[1])
    else:
        person_size = None
    
    cellM = sheet.cell(rx, 12)
    shose_size = str(cellM.value).split(':')[0].replace(".0","")
    try:
        shose_size = float(shose_size)
    except ValueError:
        shose_size = None
        
    id_person = insert_info(matricule,grade,first_name,last_name,sex,compagny,hand_width,head_measurement,waist_size,inseam_length,person_size,chest_size,shose_size)
 
    cellU = sheet.cell(rx,20)
    casque_felin = str(cellU.value).split(':')[0].replace(".0","")
    if casque_felin:
        insert_casque_felin(casque_felin,id_person)
    
#---------------------------------------------#
#---------- Insertion des matériels ----------#
#---------------------------------------------#

filepath = "bdd/res/FICHIER MODEL RECENSEMENT SMI  De  la  972° CPE  du  01 OCT  2018 .xls"

book = xlrd.open_workbook(filepath)
sheet = book.sheet_by_index(0)

for rx in range(16, sheet.nrows):
    cellE = sheet.cell(rx,4)
    qt =  str(cellE.value).split(':')[0].replace(".0","")
    if qt:
        qt = int(qt)
        cellB = sheet.cell(rx, 1)
        reference = str(cellB.value).split(':')[0]
        
        cellC = sheet.cell(rx, 2)
        designation = str(cellC.value).split(':')[0]
        
        cellA = sheet.cell(rx, 0)
        category = str(cellA.value).split(':')[0]
        
        insert_material(qt,reference,designation,category)
        
liste_material = [
    ("MASSE","GON 388","MASQUE BALISTIQUE COMBAT ET PROTECTION",30),
    ("HABILLEMENT","8470SH0008635","GILET PARE-BALLES S3 COMPLET TE TGT",5),
    ("HABILLEMENT","8470SH0008636","GILET PARE-BALLES S3 COMPLET TE PT",5),
    ("HABILLEMENT","8470SH0008637","GILET PARE-BALLES S3 COMPLET TE MT",15),
    ("HABILLEMENT","8470SH0008638","GILET PARE-BALLES S3 COMPLET TE GT",15),
    ("HABILLEMENT","8470SH0008639","GILET PARE-BALLES S3 COMPLET TE TTP",5),
    ("CAMPEMENT","7240SH0000627","NOURRICE A EAU DE 20LITRES",20),
    ("MASSE","0000-00-0006105","ECHELLE SIMPLE",5),
    ("MASSE","0099-AG-0000044","CLE DYNAMOMETRIQUE",4),
    ("MASSE","3920-BC-0100069","TRANSPALETTE",2),
    ("MASSE","5180-BC-0100117","LOT D'OUTILLAGE",20),
    ("MASSE","6115-BC-0100142","GROUPE ELECTROGENE THERMIQUE",2),
    ("MASSE","6605-BC-0100147","GPS",5),
    ("CAMPEMENT","7240-14-5523701","NOURRICE A EAU DE 20 LITRES",20),
    ("","5130-BC-0100099","VISSEUSE",5),
    ("","6625-BC-0100151","MULTICONTROLEUR",5),
    ("","9999-RD-0001733","COUPE CABLE",5),
    ("","0099-AG-0000064","KIT TESTEUR DE TERRE GEO DE BASSE",5),
    ("","0099-AG-0000127","ESCABEAU ALU PRO",5),
    ("COUCHAGE 2016","1507110","COUVERTURE POLAIRE POLECO - 180 x 220 cm",100),
    ("COUCHAGE 2016;2017;2018","1524685","TAIE  TRAVERSIN  GARNITURE  SYNTHETIQUE  BLANC  COTON   80 X 140",100),
    ("COUCHAGE 2016;2017;2018","1375576","TRAVERSIN  L 90 Cm  100 %  COTON",100),
    ("COUCHAGE 2016;2017;2018","1524685","DRAP  PLAT   COTON  BLANC - 180 X 300 Cm",200)
    ]

for row in liste_material:
   insert_material(row[3],row[1],row[2],row[0])

#-------------------------------------------------------#
#---------- Insertion des simulations de prêt ----------#
#-------------------------------------------------------#

generate_loans()
simulate_loan_status_change()
generate_loans()
simulate_loan_status_change()