import sqlite3
import datetime
import unidecode
from sqlite3 import Error
import datetime

#--------------------------------------------------#
#------- Déclaration des fonctions pour BDD -------#
#--------------------------------------------------#
def execute_query(query: str, cursor, params=None, fetch=True):
    try:
        cursor.execute(query, params or ())
        result = cursor.fetchall() if fetch else None
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        print(f"query : {query}")
        return None

def execute_insert(query: str, values: tuple, cursor)->int:
    try:
        cursor.execute(query, values)
        return cursor.lastrowid
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        print(f"query : {query} // values : {values}")
        return None
    except sqlite3.IntegrityError:
        return None
    
#---------------------------------------------------------------#
#------- Déclaration des fonctions pour la normalisation -------#
#---------------------------------------------------------------#
def norm_reference(reference):
    return unidecode.unidecode("".join(filter(str.isalnum, str(reference))))
    
def norm_designation(designation):
        return unidecode.unidecode("".join(filter(lambda c: c.isalnum() or c.isspace(), str(designation))))

def norm_matricule(matricule):
    return unidecode.unidecode("".join(filter(str.isalnum, str(matricule))))

def norm_date(date_str):
    date_formats = ['%Y-%m-%d', '%d/%m/%Y', '%m/%d/%Y']  # Incluez le format '%Y-%m-%d' si nécessaire
    for fmt in date_formats:
        try:
            return datetime.datetime.strptime(date_str, fmt).strftime("%Y-%m-%d")
        except ValueError:
            continue
    return None


#--------------------------------------------------#
#------- Déclaration des fonctions d'INSERT -------#
#--------------------------------------------------#

#------- material -------#
def insert_material(cursor, reference:str, expendable:bool = None)->int:
    reference = str(reference)
    expendable = bool(expendable)
    reference = norm_reference(reference)
    if expendable:
        param = (reference,expendable)
        query = " INSERT INTO material(reference,expendable) VALUES(?,?)"
    else:
        param = (reference,)
        query = " INSERT INTO material(reference) VALUES(?)"
    id_product = execute_insert(query,param,cursor)
    return id_product

#------- validity_date -------#
def insert_validity_date(cursor, id_material:int, validity_date:datetime )->int:
    validity_date = norm_date(validity_date)
    query = "INSERT INTO validity_date(id_material,validity_date) VALUES (?,?)"
    id_validity_date = execute_insert(query,(id_material,validity_date),cursor)
    return id_validity_date

#-------- designation -------#
def insert_designation(cursor, id_material:int, designation:str = None)->int:
    id_material = int(id_material)
    designation = str(designation)
    if designation:
        normalized_designation = norm_designation(designation)
        query = "INSERT INTO designation(id_material,designation,standardized_designation) VALUES (?,?,?)"
        values = (id_material, designation, normalized_designation)
    else:
        query ="INSERT INTO designation(id_material) VALUES (?)"
        values = (id_material,)
    id_designation = execute_insert(query,values,cursor)
    return id_designation

#------- description -------#
def insert_description(cursor, id_material:int, description:str = None)->int:
    id_material = int(id_material)
    description = str(description)
    if description:
        query = "INSERT INTO description(description,id_material) VALUES (?,?)"
        values = (description,id_material)
    else:
        query = "INSERT INTO description(id_material) VALUES (?)"
        values = (id_material,)
    id_description = execute_insert(query,values,cursor)
    return id_description

#------- category -------#
def insert_category(cursor, name:str, id_category_parent:int = None)->int:
    name = str(name)
    if id_category_parent:
        id_category_parent = int(id_category_parent)
    if name == '':
        return None
    normalized_name = norm_designation(name)
    if id_category_parent:
        param = (name,normalized_name,id_category_parent)
        query = 'INSERT INTO category(name,standardized_name,category_parent) VALUES (?,?,?)'
    else:
        param =  param = (name,normalized_name)
        query = 'INSERT INTO category(name,standardized_name) VALUES (?,?)'
    id_category = execute_insert(query, param,cursor)
    return id_category

#------- mat_cat -------#
def insert_mat_cat(cursor, id_material:int, id_category:int)->int:
    id_material = int(id_material)
    id_category = int(id_category)
    query = "INSERT INTO mat_cat(id_material,id_category) VALUES (?,?)"
    return execute_insert(query,(id_material,id_category),cursor)

#------- person -------#
def insert_person(cursor, matricule:str)->int:
    matricule = str(matricule)
    matricule = norm_matricule(matricule)
    query = "INSERT INTO person(matricule) VALUES (?)"
    id_person = execute_insert(query,(matricule,),cursor)
    return id_person

#------- identity -------#
def insert_identity(cursor,**kwargs)->int:
    if 'id_person' not in kwargs:
        raise ValueError("Il manque l'id de la personne")
    valid_keys = ["id_person", "first_name", "last_name", "regiment", "compagny", "section", "phone", "email"]
    for key in kwargs.keys():
        if key not in valid_keys:
            raise ValueError(f"Clé invalide fournie: {key}")
    key_format = f"{','.join(k for k in kwargs.keys())}"
    value_format = f"{', '.join(':' + k for k in kwargs.keys())}"
    query = f"INSERT INTO identity({key_format}) VALUES ({value_format})"
    id_identity = execute_insert(query, kwargs, cursor)
    return id_identity
 
 #------- size -------#
def insert_size(cursor, **kwargs) -> int:
    decimal_keys = ["hand_width", "head_measurement", "waist_size", "inseam_length", "person_size", "chest_size", "shose_size"]
    if "id_person" not in kwargs:
        raise ValueError("Il manque l'id de la personne.")
    key_format = ",".join(kwargs.keys())
    value_format = ",".join(f":{k}" for k in kwargs.keys())
    for key, value in kwargs.items():
        if key in decimal_keys:
            if not isinstance(value, (int, float, type(None))):
                raise ValueError(f"La valeur pour {key} doit être un nombre décimal.")
    query = f"INSERT INTO size({key_format}) VALUES ({value_format})"
    id_identity = execute_insert(query, kwargs, cursor)
    return id_identity

#------- borrowed -------#
def insert_borrowed(cursor, **kwargs) -> int:
    datetime_keys = ["loan_date", "return_date", "deadline"]
    if not "id_person" in kwargs.keys() or not "loan_date" in kwargs.keys() :
        raise ValueError("Il manque l'id de la personne ou la date pour d'emprunt")
    if not "status" in kwargs.keys():
        kwargs["status"] = "on hold"
    if kwargs["status"] not in ["on hold","accepted","refused","in progress","delay","close"]:
        raise ValueError("Le status n'est pas valide")
    for key in datetime_keys:
        if key in kwargs and kwargs[key] is not None:
            normalized_date = norm_date(kwargs[key])
            if normalized_date is None:
                raise ValueError(f"La date fournie pour {key} n'est pas valide ou dans un format incorrect.")
            kwargs[key] = normalized_date
    key_format = ",".join(kwargs.keys())
    value_format = ",".join(f":{k}" for k in kwargs.keys())
    query = f"INSERT INTO borrowed({key_format}) VALUES ({value_format})"
    id_borrowed = execute_insert(query, kwargs, cursor)
    return id_borrowed

#------- borrowed_material -------#
def insert_borrowed_material(cursor, id_material:int, id_borrowed:int)->int:
    id_material = int(id_material)
    id_borrowed = int(id_borrowed)
    query = "INSERT INTO borrowed_material(id_material,id_borrowed) VALUES (?,?)"
    id_borrowed_material = execute_insert(query,(id_material,id_borrowed),cursor)
    return id_borrowed_material

#------- status_report -------#
def insert_status_report(cursor,**kwargs)->int:
    verif = 0
    for key in kwargs.keys():
        if not key in ["text_report","id_borrowed","id_material","id_borrowed_material"]:
            raise ValueError(f"{key} ne fait pas partie des parammetres")            
        if key == "id_material" or key == "id_borrowed" or key == "id_borrowed_material":
            verif +=1
    if verif == 0 :
       raise ValueError("Il manque l'id_material, id_borrowed ou id_borrowed_material")
    elif verif >1:
        raise ValueError("un seul parametre doit être sélectionné: l'id_material, id_borrowed ou id_borrowed_material") 
    key_format = ','.join(k for k in kwargs.keys())
    value_format = ', '.join(':' + k for k in kwargs.keys())
    query = f"INSERT INTO status_report({key_format}) VALUES ({value_format})"
    id_status_report = execute_insert(query, kwargs, cursor)
    return id_status_report

#------- comment -------#
def insert_comment(cursor, id_borrowed:int, text_comment:str)->int:
    id_borrowed = int(id_borrowed)
    text_comment = str(text_comment)
    query ="INSERT INTO comment(id_borrowed, text_comment) VALUES (?,?)"
    id_comment = execute_insert(query,(id_borrowed,text_comment),cursor)
    return id_comment

#---------------------------------------------------#
#------- Déclaration des fonctions de DELETE -------#
#---------------------------------------------------#

#------- material -------#
def delete_material(cursor, id_material:int)->int:
    if not id_material:
        raise ValueError("Il manque l'id_material")
    if not isinstance(id_material,int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE material SET deleted = 1 WHERE material.id = (?) RETURNING id;"
    result = execute_query(query_update,cursor,(id_material,))
    if result:
        return result[0][0]
    else:
        raise ValueError(f"le materiel id: {id_material}, n'est pas supprimé")

#------- validity_date -------#
def delete_validity_date(cursor, id_validity_date:int)->int:
    if not id_validity_date:
        raise ValueError("id_validity absent")
    if not isinstance(id_validity_date,int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE validity_date SET deleted = 1 WHERE validity_date.id = (?) RETURNING id;"
    result = execute_query(query_update,cursor,(id_validity_date,))
    if result:
        return result[0][0]
    else:
        raise ValueError(f"la validity_date id: {id_validity_date}, n'est pas supprimé")

#------- designation -------#
def delete_designation(cursor, id_designation:int)->int:
    if not id_designation:
        raise ValueError("id_designation absent")
    if not isinstance(id_designation,int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE `designation` SET deleted = 1 WHERE designation.id = (?) RETURNING id;"
    result = execute_query(query_update,cursor,(id_designation,)) 
    if result:
        return result[0][0]
    else:
        raise ValueError(f"la designation id: {id_designation}, n'est pas supprimé")

#------- description -------#
def delete_description(cursor, id_description:int)->int:
    if not id_description:
        raise ValueError("id_description absent")
    if not isinstance(id_description,int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE `description` SET deleted = 1 WHERE description.id = (?) RETURNING id;"
    result = execute_query(query_update,cursor,(id_description,))
    if result:
        return result[0][0]
    else:
        raise ValueError(f"la description id: {id_description}, n'est pas supprimé")

#------- category -------#
def delete_category(cursor, id_category:int)->int:
    if not id_category:
        raise ValueError("id_category absent")
    if not isinstance(id_category,int):
        raise ValueError("La valeur doit être un INT")
    query_delete = "DELETE FROM `category` WHERE category.id = (?) RETURNING id;"
    result = execute_query(query_delete,cursor,(id_category,))
    if result:
        return result[0][0]
    else:
        raise ValueError(f"la category id: {id_category}, n'est pas supprimé")

#------- mat_cat -------#
def delete_mat_cat(cursor, id_material:int, id_category:int):
    if not id_category or not id_material:
        raise ValueError("id_category ou id_material absent")
    if not isinstance(id_category,int) or not isinstance(id_category,int) :
        raise ValueError("Les valeurs doivent être un INT")
    query_delete = "DELETE FROM `mat_cat` WHERE mat_cat.id_category = (?) and mat_cat.id_material = (?) RETURNING id_category, id_material;"
    result = execute_query(query_delete,cursor,(id_category,id_material))
    if result:
        return result[0]
    else:
        raise ValueError(f"mat_cat, n'est pas supprimé")

#------- person -------#
def delete_person(cursor, id_person:int)->int:
    if not id_person:
        raise ValueError("id_person absent")
    if not isinstance(id_person,int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE person SET deleted = 1 WHERE person.id = (?) RETURNING id;"
    result = execute_query(query_update,cursor,(id_person,))
    if result:
        return result[0][0]
    else:
        raise ValueError(f"la person id: {id_person}, n'est pas supprimé")

#------- identity -------#
def delete_identity(cursor, id_identity=None, id_person=None):
    if not id_identity and not id_person:
        raise ValueError("id_identity et id_person absent")
    if id_identity:
        if not isinstance(id_identity, int):
            raise ValueError("La valeur pour id_identity doit être un INT")
        query_update = "DELETE FROM `identity` WHERE `id` = ? RETURNING id;"
        param = (id_identity,)
    elif id_person:
        if not isinstance(id_person, int):
            raise ValueError("La valeur pour id_person doit être un INT")
        query_update = "DELETE FROM `identity` WHERE `id_person` = ? RETURNING id;"
        param = (id_person,)
    result = execute_query(query_update, cursor, param)
    if result:
        return result[0][0]
    else:
        raise ValueError("L'identité spécifiée n'a pas été supprimée")

#------- size -------#
def delete_size(cursor, id_size=None, id_person=None):
    if not id_size and not id_person:
        raise ValueError("id_size et id_person absent")
    if id_size:
        if not isinstance(id_size, int):
            raise ValueError("La valeur pour id_size doit être un INT")
        query_update = "DELETE FROM `size` WHERE `id` = ? RETURNING id;"
        param = (id_size,)
    elif id_person:
        if not isinstance(id_person, int):
            raise ValueError("La valeur pour id_person doit être un INT")
        query_update = "DELETE FROM `size` WHERE `id_person` = ? RETURNING id;"
        param = (id_person,)
    result = execute_query(query_update, cursor, param)
    if result:
        return result[0][0]
    else:
        raise ValueError(f"La taille spécifiée n'a pas été supprimée")
    
#------- borrowed -------#
def delete_borrowed(cursor, id_borrowed:int)->int:
    if not id_borrowed:
        raise ValueError("id_borrowed absent")
    if not isinstance(id_borrowed, int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE `borrowed` SET `status` = 'cancel' WHERE `id` = ? RETURNING `id`;"
    result = execute_query(query_update, cursor, (id_borrowed,))
    if result:
        return result[0][0]
    else:
        raise ValueError(f"borrowed id: {id_borrowed}, n'est pas supprimé")

#------- borrowed_material -------#
def delete_borrowed_material(cursor, id_borrowed_material=None, id_material=None, id_borrowed=None)->int:
    if not id_borrowed_material and (not id_material or not id_borrowed):
        raise ValueError("id_borrowed_material, id_material ou id_borrowed absent")
    if id_borrowed_material:
        if not isinstance(id_borrowed_material, int):
            raise ValueError("La valeur pour id_borrowed_material doit être un INT")
        query_update = "UPDATE borrowed_material SET deleted = 1 WHERE id = ? RETURNING id;"
        param = (id_borrowed_material,)
    elif id_material and id_borrowed:
        if not isinstance(id_material, int) or not isinstance(id_borrowed, int):
            raise ValueError("Les valeurs pour id_material et id_borrowed doivent être des INT")
        query_update = "UPDATE borrowed_material SET deleted = 1 WHERE id_material = ? AND id_borrowed = ? RETURNING id;"
        param = (id_material, id_borrowed)
    else:
        raise ValueError("Paramètres incorrects ou insuffisants")
    result = execute_query(query_update, cursor, param)
    if result:
        return result[0][0]
    else:
        raise ValueError("Le borrowed_material n'est pas supprimé")

#------- status_report-------#
def delete_status_report(cursor, id_status_report:int)->int:
    if not id_status_report:
        raise ValueError("id_status_report absent")
    if not isinstance(id_status_report, int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE `status_report` SET deleted = 1 WHERE `id` = ? RETURNING `id`;"
    result = execute_query(query_update, cursor, (id_status_report,))
    if result:
        return result[0][0]
    else:
        raise ValueError("Le status_report n'est pas supprimé")

#------- comment-------#
def delete_comment(cursor, id_comment:int):
    if not id_comment:
        raise ValueError("id_comment absent")
    if not isinstance(id_comment, int):
        raise ValueError("La valeur doit être un INT")
    query_update = "UPDATE `comment` SET deleted = 1 WHERE `id` = ? RETURNING `id`;"
    result = execute_query(query_update, cursor, (id_comment,))
    if result:
        return result[0][0]
    else:
        raise ValueError("Le comment n'est pas supprimé")
    
#--------------------------------------------------#
#------- Déclaration des fonctions d'Update -------#
#--------------------------------------------------#

#------- material -------#
def insert_material(cursor, reference:str, expendable:bool = None)->int:
    reference = str(reference)
    expendable = bool(expendable)
    reference = norm_reference(reference)
    if expendable:
        param = (reference,expendable)
        query = " INSERT INTO material(reference,expendable) VALUES(?,?)"
    else:
        param = (reference,)
        query = " INSERT INTO material(reference) VALUES(?)"
    id_product = execute_insert(query,param,cursor)
    return id_product

#------- validity_date -------#
def insert_validity_date(cursor, id_material:int, validity_date:datetime )->int:
    validity_date = norm_date(validity_date)
    query = "INSERT INTO validity_date(id_material,validity_date) VALUES (?,?)"
    id_validity_date = execute_insert(query,(id_material,validity_date),cursor)
    return id_validity_date

#-------- designation -------#
def insert_designation(cursor, id_material:int, designation:str = None)->int:
    id_material = int(id_material)
    designation = str(designation)
    if designation:
        normalized_designation = norm_designation(designation)
        query = "INSERT INTO designation(id_material,designation,standardized_designation) VALUES (?,?,?)"
        values = (id_material, designation, normalized_designation)
    else:
        query ="INSERT INTO designation(id_material) VALUES (?)"
        values = (id_material,)
    id_designation = execute_insert(query,values,cursor)
    return id_designation

#------- description -------#
def insert_description(cursor, id_material:int, description:str = None)->int:
    id_material = int(id_material)
    description = str(description)
    if description:
        query = "INSERT INTO description(description,id_material) VALUES (?,?)"
        values = (description,id_material)
    else:
        query = "INSERT INTO description(id_material) VALUES (?)"
        values = (id_material,)
    id_description = execute_insert(query,values,cursor)
    return id_description

#------- category -------#
def insert_category(cursor, name:str, id_category_parent:int = None)->int:
    name = str(name)
    if id_category_parent:
        id_category_parent = int(id_category_parent)
    if name == '':
        return None
    normalized_name = norm_designation(name)
    if normalized_name != ' ':
        if id_category_parent:
            param = (name,normalized_name,id_category_parent)
            query = 'INSERT INTO category(name,standardized_name,category_parent) VALUES (?,?,?)'
        else:
            param =  param = (name,normalized_name)
            query = 'INSERT INTO category(name,standardized_name) VALUES (?,?)'
        id_category = execute_insert(query, param,cursor)
        return id_category

#------- mat_cat -------#
def insert_mat_cat(cursor, id_material:int, id_category:int)->int:
    id_material = int(id_material)
    id_category = int(id_category)
    query = "INSERT INTO mat_cat(id_material,id_category) VALUES (?,?)"
    return execute_insert(query,(id_material,id_category),cursor)

#------- person -------#
def insert_person(cursor, matricule:str)->int:
    if matricule:
        matricule = str(matricule)
        matricule = norm_matricule(matricule)
        query = "INSERT INTO person(matricule) VALUES (?)"
        id_person = execute_insert(query,(matricule,),cursor)
        return id_person
    return None

#------- identity -------#
def insert_identity(cursor,**kwargs)->int:
    for key in kwargs.keys(): 
        if not key in ["id_person","grade","first_name","last_name","sex","regiment","compagny","section","phone","email"]:
            raise ValueError()
        if not "id_person" in kwargs.keys():
            raise ValueError("Il manque l'id de la personne")
    key_format = f"{','.join(k for k in kwargs.keys())}"
    value_format = f"{', '.join(':' + k for k in kwargs.keys())}"
    query = f"INSERT INTO identity({key_format}) VALUES ({value_format})"
    id_identity = execute_insert(query, kwargs, cursor)
    return id_identity
 
 #------- size -------#
def insert_size(cursor, **kwargs) -> int:
    decimal_keys = ["hand_width", "head_measurement", "waist_size", "inseam_length", "person_size", "chest_size", "shose_size"]
    if "id_person" not in kwargs:
        raise ValueError("Il manque l'id de la personne.")
    key_format = ",".join(kwargs.keys())
    value_format = ",".join(f":{k}" for k in kwargs.keys())
    for key, value in kwargs.items():
        if key in decimal_keys:
            if not isinstance(value, (int, float, type(None))):
                raise ValueError(f"La valeur pour {key} doit être un nombre décimal.")
    query = f"INSERT INTO size({key_format}) VALUES ({value_format})"
    id_identity = execute_insert(query, kwargs, cursor)
    return id_identity

#------- borrowed -------#
def insert_borrowed(cursor, **kwargs) -> int:
    datetime_keys = ["loan_date", "return_date", "deadline"]
    if not "id_person" in kwargs.keys() or not "loan_date" in kwargs.keys() :
        raise ValueError("Il manque l'id de la personne ou la date pour d'emprunt")
    if not "status" in kwargs.keys():
        kwargs["status"] = "on hold"
    if kwargs["status"] not in ["on hold","accepted","refused","in progress","delay","close"]:
        raise ValueError("Le status n'est pas valide")
    for key in datetime_keys:
        if key in kwargs and kwargs[key] is not None:
            normalized_date = norm_date(kwargs[key])
            if normalized_date is None:
                raise ValueError(f"La date fournie pour {key} n'est pas valide ou dans un format incorrect.")
            kwargs[key] = normalized_date
    key_format = ",".join(kwargs.keys())
    value_format = ",".join(f":{k}" for k in kwargs.keys())
    query = f"INSERT INTO borrowed({key_format}) VALUES ({value_format})"
    id_borrowed = execute_insert(query, kwargs, cursor)
    return id_borrowed

#------- borrowed_material -------#
def insert_borrowed_material(cursor, id_material:int, id_borrowed:int)->int:
    id_material = int(id_material)
    id_borrowed = int(id_borrowed)
    query = "INSERT INTO borrowed_material(id_material,id_borrowed) VALUES (?,?)"
    id_borrowed_material = execute_insert(query,(id_material,id_borrowed),cursor)
    return id_borrowed_material

#------- status_report -------#
def insert_status_report(cursor,**kwargs)->int:
    verif = 0
    for key in kwargs.keys():
        if not key in ["text_report","id_borrowed","id_material","id_borrowed_material"]:
            raise ValueError(f"{key} ne fait pas partie des parammetres")            
        if key == "id_material" or key == "id_borrowed" or key == "id_borrowed_material":
            verif +=1
    if verif == 0 :
       raise ValueError("Il manque l'id_material, id_borrowed ou id_borrowed_material")
    elif verif >1:
        raise ValueError("un seul parametre doit être sélectionné: l'id_material, id_borrowed ou id_borrowed_material") 
    key_format = ','.join(k for k in kwargs.keys())
    value_format = ', '.join(':' + k for k in kwargs.keys())
    query = f"INSERT INTO status_report({key_format}) VALUES ({value_format})"
    id_status_report = execute_insert(query, kwargs, cursor)
    return id_status_report

#------- comment -------#
def insert_comment(cursor, id_borrowed:int, text_comment:str)->int:
    id_borrowed = int(id_borrowed)
    text_comment = str(text_comment)
    query ="INSERT INTO comment(id_borrowed, text_comment) VALUES (?,?)"
    id_comment = execute_insert(query,(id_borrowed,text_comment),cursor)
    return id_comment

#--------------------------------------------------#
#------- Déclaration des fonctions d'SELECT -------#
#--------------------------------------------------#
#------- category -------#
def select_category(cursor, id_category:int = None, name_category:str = None)->dict:
    if id_category:
        id_category = int(id_category)
        query = f"SELECT * FROM category WHERE id = {id_category}"
    elif name_category:
        name_category = norm_designation(str(name_category))
        query = f"SELECT * FROM category WHERE standardized_name LIKE '{name_category}'"
    else:
        raise ValueError("Il faut l'id ou le nom de la catégorie")
    return execute_query(query, cursor)

#------- material -------#
def select_material(cursor, id_material:int = None, reference:str = None, designation:str = None, group_by_reference:bool = None, group_by_desigantion:bool = None):
    if id_material:
        condition =  f" AND mat.id = {id_material}"
    elif reference:
        reference = norm_reference(reference)
        condition = f" AND mat.reference = '{reference}'"
    elif designation:
        designation = norm_designation(designation)
        condition = f" AND desi.standardized_designation LIKE '%{designation}%'"
    else:
        condition = ""
        
    if group_by_reference == True:
        group_by = " GROUP BY mat.reference"
    elif group_by_desigantion ==True:
        group_by = " GROUP BY desi.designation"
    elif group_by_reference == True and group_by_desigantion == True:
        raise ValueError ("Impossible de groupe par reférence et désignation en même temps")
    else:
        group_by = ""
        
    query = f"""SELECT 
                mat.id,
                mat.reference,
                mat.validity_date,
                desi.designation,
                desc.description
            FROM 
                material AS mat
            LEFT JOIN 
                designation AS desi ON mat.id = desi.id_material AND desi.deleted = 0
            LEFT JOIN 
                description AS desc ON mat.id = desc.id_material AND desc.deleted = 0
            WHERE 
                mat.deleted = 0 
                 {condition} 
                 {group_by};"""
    return execute_query(query, cursor)

#------- borrowed -------#
def select_borrowed(cursor, id_borrewed:int = None, id_person:int = None, id_material:int = None,
                    reference_material:str = None, designation_material:str = None):
    if id_borrewed:
        condition = f" WHERE bor.id = {id_borrewed}"
    elif id_person:
        condition = f" WHERE bor.id = {id_person}"
    elif id_material:
        condition = f" WHERE bom.id_material = {id_material}"
    elif reference_material:
        reference_material = norm_reference(reference_material)
        condition = f" WHERE mat.reference like '%{reference_material}%'"
    elif designation_material:
        designation_material = norm_designation(designation_material)
        condition = f" WHERE desi.standardized_designation like '%{designation_material}%'"
    else:
        condition = ""
        
    query = f"""SELECT 
                    bor.id as id_borrowed,
                    bor.loan_date as loan_date,
                    bor.deadline as deadline,
                    bor.return_date as return_date,
                    bor.status as status,
                    bom.id_material as id_material,
                    mat.reference as reference_material,
                    desi.designation as designation_material,
                    desc.description as description_material
                FROM
                    borrowed as bor
                LEFT JOIN
                    borrowed_material AS bom ON bor.id = bom.id_borrowed AND bom.deleted = 0
                LEFT JOIN
                    material AS mat ON bom.id_material = mat.id
                LEFT JOIN
                    designation AS desi ON mat.id = desi.id_material AND desi.deleted = 0
                LEFT JOIN
                    description AS desc ON mat.id = desc.id_material AND desc.deleted = 0
                {condition}
            """
    return execute_query(query,cursor)


#-----------------------------------------------#
#------- Déclaration des fonctions COUNT -------#
#-----------------------------------------------#

def count_material(cursor, reference:str = None, designation:str = None):
    if reference:
        liste = select_material(cursor, reference = reference)
        return len(liste)
    elif designation:
        liste = select_material(cursor, designation = designation)
        return len(liste)    