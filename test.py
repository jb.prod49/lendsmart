import model.db_fonction as db_f
import pytest

#------------------------------------------#
#------- tests des fonctions INSERT -------#
#------------------------------------------#
# pytest test.py
#------- material -------#
def test_insert_material():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    for ref in ["RéF123", "REF456", "REF789@e", "1011", "REF1213", "REF1415", "REF1617", "REF1819", "REF2021", "REF2223"]:
        result.append(db_f.insert_material(cur, ref))
        assert isinstance(result[-1], int)
        assert result[-1] not in result[:-1]
    conn.commit()
    conn.close() 

#------- validity_date -------#       
def test_insert_validity_date():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA strict=ON')
    cur = conn.cursor()
    result = []
    for param in [("1", "22/01/2025"), 
                  ("2","22/10/2024"), 
                  ("3","21/02/2025"), 
                  ("1","2125/02/20"), 
                  ("4","2125/02/20"),
                  ("4","2205/02/20"),
                  ("3","10/23/2025")]:
        print(param)
        result.append(db_f.insert_validity_date(cur,param[0],param[1]))
        assert isinstance(result[-1], int)
        assert result[-1] not in result[:-1]
    conn.commit()
    conn.close() 

#------- designation -------#       
def test_insert_designation():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    cur = conn.cursor()
    result = []
    for param in [(1, 'Marteau'),(2, 'Tournevis'),(3, 'Scie'),(1, 'Clé à molette'),(3, 'Pince')]:
        print(param)
        result.append(db_f.insert_designation(cur,param[0],param[1]))
        assert isinstance(result[-1], int)
        assert result[-1] not in result[:-1]
    conn.commit()
    conn.close()    

#------- description -------#       
def test_insert_description():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    for param in [(1, 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'),
                  (2, 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'),
                  (3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id neque nulla. Phasellus pharetra arcu at nunc fringilla, in finibus arcu vehicula. Aliquam et est consectetur, ultrices orci et, vulputate eros. Etiam id nisi sed ante elementum efficitur. Maecenas pulvinar erat leo, eu condimentum nibh tincidunt at. Praesent eleifend lorem in ante imperdiet dictum. Nam eleifend justo eu placerat lacinia. Sed ultrices magna eget ipsum aliquet, ut hendrerit nisi viverra. Sed vehicula nec velit ac volutpat. Sed sed scelerisque nulla. Donec id mauris nec leo tempor venenatis. Mauris euismod euismod tellus, eu sagittis sapien fringilla vitae. Phasellus ut elit diam. Phasellus nunc lorem, blandit a nisi vel, imperdiet varius tortor. '),
                  (1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id condimentum erat. Morbi sed nulla et lacus porta rutrum. Fusce rutrum odio elit, sit amet tempor massa feugiat a. Nam vestibulum purus quis mi molestie mollis. Nam commodo eu sapien ac luctus. Nullam ac maximus massa. Donec sem diam, vulputate tincidunt libero nec, molestie dapibus justo. Sed eu nisi vitae risus euismod facilisis a in erat. Curabitur sit amet tempor massa, in tincidunt erat. Sed condimentum sapien lorem, interdum porttitor nulla porttitor in. In bibendum interdum dui vel tincidunt. Mauris a euismod velit. Quisque sit amet tortor neque.'),
                  (4, ''),
                  (3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id condimentum erat. Morbi sed nulla et lacus porta rutrum. Fusce rutrum odio elit, sit amet tempor massa feugiat a. Nam vestibulum purus quis mi molestie mollis. Nam commodo eu sapien ac luctus. Nullam ac maximus massa. Donec sem diam, vulputate tincidunt libero nec, molestie dapibus justo. Sed eu nisi vitae risus euismod facilisis a in erat. Curabitur sit amet tempor massa, in tincidunt erat. Sed condimentum sapien lorem, interdum porttitor nulla porttitor in. In bibendum interdum dui vel tincidunt. Mauris a euismod velit. Quisque sit amet tortor neque.')]:
        print(param)
        result.append(db_f.insert_description(cur,param[0],param[1]))
        assert isinstance(result[-1], int)
        assert result[-1] not in result[:-1]
    conn.commit()
    conn.close() 
    
#------- category -------#       
def test_insert_category():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    for param in [('Outils à main',None),('Marteaux',1),('Tournevis',1),('clés à pipe',1),('Outils électriques',None),('Perceuses',5),('Pinces',1)]:
        print(param)
        result.append(db_f.insert_category(cur,param[0],param[1]))
        assert isinstance(result[-1], int)
        assert result[-1] not in result[:-1]
    conn.commit()
    conn.close() 
        
#------- mat_cat -------#       
def test_insert_mat_cat():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    for param in [(1,1),
                  (1,2),
                  (2,2),
                  (2,4),
                  (10,7),
                  (5,5)]:
        print(param)
        result.append(db_f.insert_mat_cat(cur,param[0],param[1]))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 

#------- person -------#       
def test_insert_person():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    for param in ['AB1234','987ZXCV','1234567','abCDefg','AB-1234','987-ZXCV','12#34@56',
                  'abc*def&','AB 1234','987 ZXCV','12 34 56','abc def g','AbcDe123',
                  'xYz9876','GhI456Jk','123456','654321','ABCDEFG','hijklmn','FR-AB 123 CD',
                  'DE12 AB34','US_456_7890','CA%987%654','>>AB>>123','<<ZX>>789','**12**345',
                  'A1B2C3','4D5E6F','ÉÀÈ123','ñçø456']:
        print(param)
        result.append(db_f.insert_person(cur,param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 

#------- identity -------#       
def test_insert_identity():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    test_cases = [
                    {"id_person": 1, "first_name": "John", "last_name": "Doe", "phone": 1234567890, "email": "john@example.com", "regiment": "Infantry"},
                    {"first_name": "Jane"},
                    {"id_person": 2, "first_name": "Jane", "last_name": "Doe", "phone": "not_a_number", "email": "jane@example.com"},
                    {"id_person": 3, "first_name": "John", "last_name": "Doe", "phone": 12345678901234567890, "email": "longphone@example.com"},
                    {"id_person": 4, "first_name": "Julie", "last_name": "Doe", "regiment": "Infantry"},
                    {"id_person": 5, "first_name": "Jim ", "last_name": " Doe", "phone": 1234567890, "email": "jim@example.com"},
                    {"id_person": 6, "first_name": "Jack", "last_name": "Doe", "phone": 1.234567890, "email": "jack@example.com"},
                    {"id_person": 7, "first_name": "Joan", "last_name": "Doe", "phone": 1234567890, "email": "joan@example.com", "age": 30},
                    {"id_person": 1, "first_name": "Duplicate", "last_name": "Person", "phone": 1234567890, "email": "duplicate@example.com"},
                    {"id_person": 8, "first_name": "Empty", "last_name": "Fields", "regiment": None}
                ]
    for param in test_cases:
        print(param)
        result.append(db_f.insert_identity(cur,**{'id_person':param[0],'first_name':param[1],'last_name':param[2],'adress':param[3],'zipcode':param[4],'city':param[5],'phone':param[6],'email':param[7]}))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 

#------- size -------#       
def test_insert_size():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[
        # Exemples de données valides
            {"id_person": 1, "hand_width": 8.5, "head_measurement": 56, "waist_size": 32, "inseam_length": 33, "person_size": 180, "chest_size": 42, "shose_size": 9},
            {"id_person": 2, "hand_width": 9.0, "head_measurement": 59, "waist_size": 36, "inseam_length": 34, "person_size": 185, "chest_size": 45, "shose_size": 10},
        # Données avec valeurs extrêmes
            {"id_person": 3, "hand_width": 12.0, "head_measurement": 70, "waist_size": 60, "inseam_length": 40, "person_size": 250, "chest_size": 60, "shose_size": 15},
            {"id_person": 4, "hand_width": 5.0, "head_measurement": 40, "waist_size": 20, "inseam_length": 20, "person_size": 140, "chest_size": 30, "shose_size": 5},
        # Données manquantes pour certains champs    
            {"id_person": 5, "hand_width": 8.0, "head_measurement": 55, "waist_size": 32, "person_size": 170, "chest_size": 40}, 
            {"id_person": 6, "head_measurement": 57, "waist_size": 30, "inseam_length": 31, "person_size": 175, "chest_size": 39, "shose_size": 8.5},
        # Données incorrectes (types erronés)    
            # {"id_person": 7, "hand_width": "large", "head_measurement": "medium", "waist_size": "small", "inseam_length": "long", "person_size": "tall", "chest_size": "broad", "shose_size": "big"},
            # {"id_person": "eight", "hand_width": 8.0, "head_measurement": 56, "waist_size": 32, "inseam_length": 33, "person_size": 180, "chest_size": 42, "shose_size": 9},
        # Combinaison de formats valides et invalides    
            {"id_person": 8, "hand_width": 8.5, "head_measurement": 56, "waist_size": 32, "inseam_length": 33, "person_size": 180, "chest_size": 42, "shose_size": 9}, 
            {"id_person": 9, "hand_width": None, "head_measurement": None, "waist_size": None, "inseam_length": None, "person_size": None, "chest_size": None, "shose_size": None}
            ]
    for param in params:
        print(param)
        result.append(db_f.insert_size(cur, **param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close()

#------- borrowed -------#       
def test_insert_borrowed():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[
            # Données complètes valides
                {"id_person": 1, "loan_date": "25/12/2024", "return_date": "30/12/2024", "deadline": "28/12/2024", "status": "on hold"},
            # Dates futuristes
                {"id_person": 2, "loan_date": "25/12/2050", "return_date": "30/12/2050", "deadline": "28/12/2050", "status": "accepted"},
            # Dates formattées incorrectement
                # {"id_person": 3, "loan_date": "2024-12-25", "return_date": "2024/30/12", "deadline": "12-28-2024", "status": "in progress"},
            # Statut manquant
                {"id_person": 4, "loan_date": "01/01/2025", "return_date": "05/01/2025", "deadline": "03/01/2025"},
            # Valeurs numériques pour les dates
                # {"id_person": 5, "loan_date": 20241225, "return_date": 20241230, "deadline": 20241228, "status": "delay"},
            # Statuts non définis
                # {"id_person": 6, "loan_date": "10/02/2025", "return_date": "15/02/2025", "deadline": "12/02/2025", "status": "unknown status"}
            ]
    for param in params:
        print(param)
        result.append(db_f.insert_borrowed(cur, **param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close()  
    
 #------- borrowed_material -------#       
def test_insert_borrowed_material():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[
            (1, 1),
            # id n'existent pas
                # (9999, 9999),
            # Valeurs limites pour INT
                # (2147483647, 2147483647),  
            # Types incorrects
                # ("abc", "xyz"),  
            # Valeurs valides multiples
                (1, 2), 
                (4, 2), 
                (4, 3),
                (4, 3),
            # Valeurs négatives 
                # (-1, -2),  
            # Test avec zéro
                # (0, 0), 
            ]
    for param in params:
        print(param)
        result.append(db_f.insert_borrowed_material(cur,param[0],param[1]))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close()  

 #------- status_report -------#       
def test_insert_status_report():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[
            # Valide
                {"text_report": "Fully functional device, no issues.", "id_borrowed": 1},  
                {"text_report": "Missing borrowed id.", "id_material": 2},  
                {"text_report": "", "id_borrowed_material": 3},
            # Numérique pour le texte 
                {"text_report": 12345, "id_material": 4},  
            # Trop de parametre
                # {"text_report": "Incorrect date format used.", "id_borrowed": 1, "id_material": 5, "id_borrowed_material": 5},  
            # Statut non valide
                # {"text_report": "Invalid status provided.","id_borrowed_material": 6, "status": "unknown"},  
            # Champs supplémentaires
                # {"text_report": "Extra field provided.", "id_borrowed": 7, "id_material": 7, "id_borrowed_material": 7, "extra_field": "Not needed"},  
            # Zéro IDs
                # {"text_report": "Zero IDs test.", "id_material": 0},  
            # Caractères spéciaux
                {"text_report": "Special characters test: @#%&*", "id_borrowed": 2}  
            ]
    for param in params:
        print(param)
        result.append(db_f.insert_status_report(cur,**param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 
    
 #------- comment -------#       
def test_insert_comment():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[
                (1, "This is a normal and valid comment."),
                # (999, "This borrowed ID does not exist."),
                (2, ""),
                (3, "Special characters #@!$%"),
                (3, "Robert'); DROP TABLE comment; --"),
                (2, "a" * 5000),
                # (0, "Testing with zero ID"),
                # (-1, "Negative ID test"),
                (1, "1234567890"),
                (1, "Multi-line\ntext\ncomment")
            ]   
    for param in params:
        print(param)
        result.append(db_f.insert_comment(cur,param[0],param[1]))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 


#------------------------------------------#
#------- tests des fonctions DELETE -------#
#------------------------------------------#

#------- material -------#
def test_delete_material():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[1, 
            #  9999, 
            #  "string", 
            #  None, 
            #  0, 
            #  -1, 
            #  2.5, 
             1, 
            #  2147483647, 
             True
             ]
    for param in params:
        print(param)
        result.append(db_f.delete_material(cur,param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 
    
#------- validity_date -------#
def test_delete_validity_date():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[1, 
            #  9999, 
            #  "string", 
            #  None, 
            #  0, 
            #  -1, 
            #  2.5, 
             4, 
            #  2147483647, 
             1
            ]
    for param in params:
        print(param)
        result.append(db_f.delete_validity_date(cur,param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 

#------- designation -------#
def test_delete_designation():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[2, 
            #  9999, 
            #  "string", 
            #  None, 
            #  0, 
            #  -1, 
            #  2.5, 
             4, 
            #  2147483647, 
             2
            ]
    for param in params:
        print(param)
        result.append(db_f.delete_designation(cur,param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 

#------- description -------#
def test_delete_ddescription():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[2, 
            #  9999, 
            #  "string", 
            #  None, 
            #  0, 
            #  -1, 
            #  2.5, 
             4, 
            #  2147483647, 
             2
            ]
    for param in params:
        print(param)
        result.append(db_f.delete_description(cur,param))
        assert isinstance(result[-1], int)
    conn.commit()
    conn.close() 
    
#------- category -------#
def test_delete_category():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[1, 
            #  9999, 
            #  "string", 
            #  None, 
            #  0, 
            #  -1, 
            #  2.5, 
             4, 
            #  2147483647, 
            #  1
            ]
    for param in params:
        print(param)
        result.append(db_f.delete_category(cur,param))
    conn.commit()
    conn.close() 

#------- mat_cat -------#
def test_delete_mat_cat():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[
            (2, 2),
            # (9999, 9999),
            # ("string", "string"),
            # (None, None), 
            # (0, 0), 
            # (-1, -1),
            # (2.5, 2.5),
            # (2, 2)
            ]
    for param in params:
        print(param)
        result.append(db_f.delete_mat_cat(cur, param[0], param[1]))
    conn.commit()
    conn.close() 
    
#------- person -------#
def test_delete_person():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    result = []
    params =[1, 
            #  9999, 
            #  "string", 
            #  None, 
            #  0, 
            #  -1, 
            #  2.5, 
             4, 
            #  2147483647, 
             1
            ]
    for param in params:
        print(param)
        result.append(db_f.delete_person(cur, param))
    conn.commit()
    conn.close() 
    
#------- identity -------#
def test_delete_identity():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    test_cases =[
                {'id_identity': 2},  # valide
                {'id_person': 6},  # valide
                # {'id_identity': 9999},  # inexistant
                # {'id_person': 9999},  # inexistant
                # {'id_identity': "invalid"},  # type incorrect
                # {'id_person': "invalid"},  # type incorrect
                # {},  # valeurs nulles
                # {'id_identity': 0},  # zéro
                # {'id_person': 0},  # zéro
                # {'id_identity': -1},  # négatif
            ]   
    for params in test_cases:
        print(params)
        if 'id_identity' in params:
            db_f.delete_identity(cur, id_identity=params['id_identity'])
        elif 'id_person' in params:
            db_f.delete_identity(cur, id_person=params['id_person'])
    conn.commit()
    conn.close() 

#------- size -------#
def test_delete_size():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    test_cases = [
                    {'id_size': 1},  # valide
                    {'id_person': 4},  # valide
                    # {'id_size': 9999},  # inexistant
                    # {'id_person': 9999},  # inexistant
                    # {'id_size': "invalid"},  # type incorrect
                    # {'id_person': "invalid"},  # type incorrect
                    # {},  # valeurs nulles
                    # {'id_size': 0},  # zéro
                    # {'id_person': 0},  # zéro
                    # {'id_size': -1},  # négatif
                    # {'id_person': -1}  # négatif
                ]
    for params in test_cases:
        print(params)
        if 'id_size' in params:
            db_f.delete_size(cur, id_size=params['id_size'])
        elif 'id_person' in params:
            db_f.delete_size(cur, id_person=params['id_person'])
    conn.commit()
    conn.close()
         
#------- borrowed -------#
def test_delete_borrowed():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    test_cases = [
        {'id_borrowed': 1},  # valide
        # {'id_borrowed': 9999},  # inexistant
        # {'id_borrowed': "invalid"},  # type incorrect
        # {'id_borrowed': None},  # valeur nulle
        # {'id_borrowed': 0},  # zéro
        # {'id_borrowed': -1},  # négatif
        # {'id_borrowed': 2.5},  # flottant
        {'id_borrowed': 1},  # double suppression
        # {'id_borrowed': 2147483647},  # grand nombre
        {'id_borrowed': 1}  # mise à jour incorrecte (déjà testé en double suppression)
    ]
    for params in test_cases:
        print(params)
        db_f.delete_borrowed(cur, params['id_borrowed'])

    conn.commit()
    conn.close()

#------- borrowed_material -------#
def test_delete_borrowed_material():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    test_cases = [
        {'id_borrowed_material': 1},  # valide
        {'id_material': 1, 'id_borrowed': 2},  # valide
        # {'id_borrowed_material': 9999},  # inexistant
        # {'id_material': 9999, 'id_borrowed': 9998},  # inexistant
        # {'id_borrowed_material': "invalid"},  # type incorrect
        # {'id_material': "text", 'id_borrowed': "text"},  # type incorrect
        # {'id_borrowed_material': None},  # valeur nulle
        # {'id_material': 0, 'id_borrowed': 0},  # zéro
        # {'id_borrowed_material': -1},  # négatif
        # {'id_material': -1, 'id_borrowed': -2}  # négatif
    ]
    for params in test_cases:
        print(params)
        if 'id_borrowed_material' in params:
            result = db_f.delete_borrowed_material(cur, id_borrowed_material=params['id_borrowed_material'])
        elif 'id_material' in params and 'id_borrowed' in params:
            result = db_f.delete_borrowed_material(cur, id_material=params['id_material'], id_borrowed=params['id_borrowed'])

    conn.commit()
    conn.close()

#------- status_report -------#
def test_delete_status_report():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    test_cases = [
        (2),  # valide
        # (9999),  # inexistant
        # ("invalid"),  # type incorrect
        # (None),  # valeur nulle
        # (0),  # zéro
        # (-1),  # négatif
        (2),  # double suppression
        # (1.5),  # flottant
        (2),  # already deleted
        (3),
        (True)  # booléen
    ]
    for id_status_report in test_cases:
        print(id_status_report)
        result = db_f.delete_status_report(cur, id_status_report)  
    conn.commit()
    conn.close()

#------- comment -------#
def test_delete_comment():
    conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
    conn.execute('PRAGMA foreign_keys = ON')
    cur = conn.cursor()
    test_cases = [
                    (1),  # valide
                    # (9999),  # inexistant
                    # ("invalid"),  # type incorrect
                    # (None),  # valeur nulle
                    # (0),  # zéro
                    # (-1),  # négatif
                    (1),  # double suppression
                    # (1.5),  # flottant
                    (1),  # already deleted
                    (2),
                    (True)  # booléen
                 ]
    for id_comment in test_cases:
        print(id_comment)
        result = db_f.delete_comment(cur, id_comment)  
        print(f"Test result for id_comment = {id_comment}: Deleted successfully, ID = {result}")
    conn.commit()
    conn.close()

    
    
# conn = db_f.sqlite3.connect("bdd/lendsmart_bdd.db")
# conn.execute('PRAGMA foreign_keys = ON')
# cur = conn.cursor()
# db_f.delete_material(cur,2)

# conn.commit()
# conn.close()